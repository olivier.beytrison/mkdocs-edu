FROM python:3

RUN apt-get update && apt-get install -y zip
RUN  pip install --upgrade pip
COPY requirements.txt /tmp/
RUN pip install --default-timeout=100 -r /tmp/requirements.txt

ENTRYPOINT ["mkdocs"]
